package by.issoft.training.springreactive.configutation;

import by.issoft.training.springreactive.dao.CategoryDao;
import by.issoft.training.springreactive.dao.MongoObject;
import by.issoft.training.springreactive.dao.ProductDao;
import by.issoft.training.springreactive.dao.UserDao;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DaoConfiguration {
    private MongoDatabase toolshopDb;

    public DaoConfiguration(@Value("${mongodb.uri}") String mongoDbUri) {
        MongoClient mongoClient = MongoClients.create(mongoDbUri);
        this.toolshopDb = mongoClient.getDatabase("toolshop");
    }

    @Bean
    public UserDao userDao() {
        return new UserDao(this.toolshopDb, MongoObject.USER);
    }

    @Bean
    public ProductDao productDao() {
        return new ProductDao(this.toolshopDb, MongoObject.PRODUCT);
    }

    @Bean
    public CategoryDao categoryDao() {
        return new CategoryDao(this.toolshopDb, MongoObject.CATEGORY);
    }
}