package by.issoft.training.springreactive.controller;

import by.issoft.training.springreactive.dto.Category;
import by.issoft.training.springreactive.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class CategoryController {
    private final CategoryService categoryService;

    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping(value = "/categories")
    public List<Category> getCategories() {
        return this.categoryService.getAll();
    }

    @GetMapping(value = "/categories/level/{level}")
    public List<Category> getCategoriesByLevel(@PathVariable(name = "level") Integer level) {
        return this.categoryService.getByLevel(level);
    }

    @GetMapping(value = "/categories/{categoryId}")
    public List<Category> getSubcategories(@PathVariable(name = "categoryId") String categoryId) {
        return this.categoryService.getSubcategories(categoryId);
    }
}
