package by.issoft.training.springreactive.controller;

import by.issoft.training.springreactive.dto.Product;
import by.issoft.training.springreactive.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class ProductController {

    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping(value = "/products")
    public List<Product> getProducts() {
        return this.productService.getAll();
    }

    @GetMapping(value = "/products/category/{categoryId}")
    public List<Product> getProductsByCategory(@PathVariable(name = "categoryId") String categoryId) {
        return this.productService.getByCategory(categoryId);
    }
}