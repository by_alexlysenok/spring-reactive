package by.issoft.training.springreactive.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Category extends BasicMongoDto {
    private String name;
    private Integer level;
    private String parentCategoryId;

    public Category(String name, Integer level, String parentCategoryId) {
        this.name = name;
        this.level = level;
        this.parentCategoryId = parentCategoryId;
    }
}
