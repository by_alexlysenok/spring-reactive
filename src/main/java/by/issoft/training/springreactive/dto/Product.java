package by.issoft.training.springreactive.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Product extends BasicMongoDto {
    private String name;
    private String model;
    private String manufacturer;
    private String categoryId;
    private List<String> details;

    public Product() {}

    public Product(String name, String model, String manufacturer, String categoryId, List<String> details) {
        this.name = name;
        this.model = model;
        this.manufacturer = manufacturer;
        this.categoryId = categoryId;
        this.details = details;
    }
}
