package by.issoft.training.springreactive.dto;

import lombok.*;

@Getter
@Setter
public class User extends BasicMongoDto {
    private String firstName;
    private String secondName;

    public User() {}

    public User(String firstName, String secondName) {
        this.firstName = firstName;
        this.secondName = secondName;
    }
}
