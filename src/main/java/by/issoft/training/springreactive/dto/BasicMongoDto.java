package by.issoft.training.springreactive.dto;

public class BasicMongoDto {
    private String objectId;

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }
}