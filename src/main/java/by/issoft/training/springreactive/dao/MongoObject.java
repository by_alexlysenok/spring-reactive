package by.issoft.training.springreactive.dao;

public enum MongoObject {
    USER("users"),
    CATEGORY("categories"),
    PRODUCT("products");

    private String collectionName;

    MongoObject(String collectionName) {
       this.collectionName = collectionName;
    }

    public String getCollectionName() {
        return collectionName;
    }
}