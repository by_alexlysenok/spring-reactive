package by.issoft.training.springreactive.dao;

import by.issoft.training.springreactive.dto.User;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

public class UserDao extends BasicMongoDao<User> {

    public UserDao(MongoDatabase database, MongoObject mongoObject)
    {
        super(database, mongoObject);
    }

    @Override
    public User convert(Document document) {
        String firstName = document.get("firstName", String.class);
        String secondName = document.get("secondName", String.class);

        return new User(firstName, secondName);
    }
}