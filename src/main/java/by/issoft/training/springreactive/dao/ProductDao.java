package by.issoft.training.springreactive.dao;

import by.issoft.training.springreactive.dto.Product;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.List;

import static com.mongodb.client.model.Filters.eq;

public class ProductDao extends BasicMongoDao<Product> {

    public ProductDao(MongoDatabase database, MongoObject mongoObject) {
        super(database, mongoObject);
    }

    @Override
    public Product convert(Document document) {
        String name = document.get("name", String.class);
        String model = document.get("model", String.class);
        String manufacturer = document.get("manufacturer", String.class);
        String categoryId = document.getObjectId("categoryId").toString();
        List<String> details = document.getList("details", String.class);

        return new Product(name, model, manufacturer, categoryId, details);
    }

    public List<Product> findByCategory(String categoryId) {
        return super.find(eq("categoryId", new ObjectId(categoryId)));
    }
}