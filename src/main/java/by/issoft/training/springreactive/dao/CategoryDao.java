package by.issoft.training.springreactive.dao;

import by.issoft.training.springreactive.dto.Category;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.List;

import static com.mongodb.client.model.Filters.eq;

public class CategoryDao extends BasicMongoDao<Category> {

    public CategoryDao(MongoDatabase database, MongoObject mongoObject) {
        super(database, mongoObject);
    }

    public List<Category> findByLevel(Integer level) {
        return super.find(eq("level", level));
    }

    public List<Category> findSubcategories(String parentCategoryId) {
        return super.find(eq("parentCategory", new ObjectId(parentCategoryId)));
    }

    @Override
    public Category convert(Document document) {
        String name = document.get("name", String.class);
        Integer level = document.get("level", Integer.class);

        String parentCategoryId;
        if (document.containsKey("parentCategory")) {
            parentCategoryId = document.getObjectId("parentCategory").toString();
        } else {
            parentCategoryId = null;
        }

        return new Category(name, level, parentCategoryId);
    }
}
