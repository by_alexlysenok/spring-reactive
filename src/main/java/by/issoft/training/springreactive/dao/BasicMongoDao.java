package by.issoft.training.springreactive.dao;

import by.issoft.training.springreactive.dto.BasicMongoDto;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.List;

public abstract class BasicMongoDao<T extends BasicMongoDto> {
    private MongoDatabase database;
    private MongoObject mongoObject;

    public BasicMongoDao(MongoDatabase database, MongoObject mongoObject) {
        this.database = database;
        this.mongoObject = mongoObject;
    }

    public MongoCollection<Document> getCollection() {
        return this.database.getCollection(this.mongoObject.getCollectionName());
    }

    public List<T> findAll() {
        return find(null);
    }

    public List<T> find(Bson filter) {
        List<T> result = new ArrayList<>();
        FindIterable<Document> documents;

        if (filter != null) {
            documents = this.getCollection().find(filter);
        } else {
            documents = this.getCollection().find();
        }

        MongoCursor<Document> cursor = documents.cursor();
        while (cursor.hasNext()) {
            result.add(this.convertObject(cursor.next()));
        }

        return result;
    }

    public abstract T convert(Document document);

    private T convertObject(Document document) {
        T converted = convert(document);
        converted.setObjectId(document.getObjectId("_id").toString());
        return converted;
    }
}