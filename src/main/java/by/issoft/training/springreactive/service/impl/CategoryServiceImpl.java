package by.issoft.training.springreactive.service.impl;

import by.issoft.training.springreactive.dao.CategoryDao;
import by.issoft.training.springreactive.dto.Category;
import by.issoft.training.springreactive.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    final private CategoryDao categoryDao;

    @Autowired
    public CategoryServiceImpl(CategoryDao categoryDao) {
        this.categoryDao = categoryDao;
    }

    @Override
    public List<Category> getAll() {
        return categoryDao.findAll();
    }

    @Override
    public List<Category> getByLevel(Integer level) {
        return categoryDao.findByLevel(level);
    }

    @Override
    public List<Category> getSubcategories(String parentCategoryId) {
        return categoryDao.findSubcategories(parentCategoryId);
    }
}
