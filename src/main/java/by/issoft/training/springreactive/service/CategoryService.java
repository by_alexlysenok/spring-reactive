package by.issoft.training.springreactive.service;

import by.issoft.training.springreactive.dto.Category;

import java.util.List;

public interface CategoryService {
    List<Category> getAll();
    List<Category> getByLevel(Integer level);
    List<Category> getSubcategories(String parentCategoryId);
}
