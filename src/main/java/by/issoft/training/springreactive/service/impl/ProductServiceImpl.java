package by.issoft.training.springreactive.service.impl;

import by.issoft.training.springreactive.dao.ProductDao;
import by.issoft.training.springreactive.dto.Product;
import by.issoft.training.springreactive.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    final private ProductDao productDao;

    @Autowired
    public ProductServiceImpl(ProductDao productDao) {
        this.productDao = productDao;
    }

    @Override
    public List<Product> getAll() {
        return productDao.findAll();
    }

    @Override
    public List<Product> getByCategory(String categoryId) {
        return productDao.findByCategory(categoryId);
    }
}