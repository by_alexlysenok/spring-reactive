package by.issoft.training.springreactive.service.impl;

import by.issoft.training.springreactive.dao.UserDao;
import by.issoft.training.springreactive.dto.User;
import by.issoft.training.springreactive.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserDao userDao;

    @Autowired
    public UserServiceImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public List<User> getAll() {
        return userDao.findAll();
    }
}