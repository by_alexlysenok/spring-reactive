package by.issoft.training.springreactive.service;

import by.issoft.training.springreactive.dto.Product;

import java.util.List;

public interface ProductService {
    List<Product> getAll();

    List<Product> getByCategory(String categoryId);
}