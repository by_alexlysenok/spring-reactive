package by.issoft.training.springreactive.service;

import by.issoft.training.springreactive.dto.User;

import java.util.List;

public interface UserService {
    List<User> getAll();
}