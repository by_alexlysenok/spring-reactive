export class Product {
    id: string;
    name: string;
    model: string;
    manufacturer: string;
}
