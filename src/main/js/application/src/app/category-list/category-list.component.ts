import { Component, OnInit } from '@angular/core';
import {Category} from "../model/category";

@Component({
  selector: 'category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.css']
})
export class CategoryListComponent implements OnInit {

  categories: Category[];

  constructor() { }

  ngOnInit(): void {
      this.categories = [
        {
          label: 'Ленточные и электрические пилы',
          img: 'https://pimdatacdn.bahco.com/media/sub351/16bdf98926a5cf3f.png'
        },
        {
          label: 'Напильники',
          img: 'https://pimdatacdn.bahco.com/media/sub1044/16a916c323dd993f.png'
        },
        {
          label: 'Гаечные ключи',
          img: 'https://pimdatacdn.bahco.com/media/sub1044/16a9171ce51dd63f.png'
        },
        {
          label: 'Головки и трещотки',
          img: 'https://pimdatacdn.bahco.com/media/sub1044/16a917013370213f.png'
        },
        {
          label: 'Динамометрические инструменты',
          img: 'https://pimdatacdn.bahco.com/media/sub1044/16a91773da78d43f.png'
        },
        {
          label: 'Аксессуары для электроинструментов',
          img: 'https://pimdatacdn.bahco.com/media/sub1044/16a916fedbffec3f.png'
        },
        {
          label: 'Отвертки',
          img: 'https://pimdatacdn.bahco.com/media/sub1044/16a916ffd50e243f.png'
        },
        {
          label: 'Г-образные ключи',
          img: 'https://pimdatacdn.bahco.com/media/sub1044/16a916e0b696ec3f.png'
        },
        {
          label: 'Плоскогубцы',
          img: 'https://pimdatacdn.bahco.com/media/sub1044/16a916fcc7d32d3f.png'
        },
        {
          label: 'Трубные ключи и инструменты для трубопроводов',
          img: 'https://pimdatacdn.bahco.com/media/sub1044/16a916f88e24603f.png'
        },
        {
          label: 'Пневматические инструменты',
          img: 'https://pimdatacdn.bahco.com/media/sub1044/16a916becb940f3f.png'
        },
        {
          label: 'Аккумуляторные инструменты',
          img: 'https://pimdatacdn.bahco.com/media/sub1044/16a916c0b6655f3f.png'
        },
        {
          label: 'Автомобильные инструменты',
          img: 'https://pimdatacdn.bahco.com/media/sub1044/16a916c03960273f.png'
        },
        {
          label: 'Осветительное оборудование',
          img: 'https://pimdatacdn.bahco.com/media/sub243/16e63a314ef3233f.png'
        },
        {
          label: 'Диагностические инструменты',
          img: 'https://pimdatacdn.bahco.com/media/sub1044/16a916deb772fd3f.png'
        },
        {
          label: 'Экстракторы и съемники',
          img: 'https://pimdatacdn.bahco.com/media/sub1044/16a916c205a5763f.png'
        },
        {
          label: 'Хранение и наборы инструментов',
          img: 'https://pimdatacdn.bahco.com/media/sub1044/16a91716106dc63f.png'
        },
        {
          label: 'Инструменты для работы на высоте',
          img: 'https://pimdatacdn.bahco.com/media/sub1044/16a91718ea01773f.png'
        },
        {
          label: 'Изолированные инструменты',
          img: 'https://pimdatacdn.bahco.com/media/sub1044/16a916dfba421c3f.png'
        },
        {
          label: 'Искробезопасные инструменты',
          img: 'https://pimdatacdn.bahco.com/media/sub1044/16a916e22ab3633f.png'
        },
        {
          label: 'Инструменты из нержавеющей стали',
          img: 'https://pimdatacdn.bahco.com/media/sub1044/16a91701e928213f.png'
        },
        {
          label: 'Пинцеты и специалированные электронные инструменты',
          img: 'https://pimdatacdn.bahco.com/media/sub1044/16a9171bd057663f.png'
        },
        {
          label: 'Измерение и разметка',
          img: 'https://pimdatacdn.bahco.com/media/sub1044/16a916e1a8d0203f.png'
        },
        {
          label: 'Инструменты для стен',
          img: 'https://pimdatacdn.bahco.com/media/sub1044/16a9171cc7ddd13f.png'
        },
        {
          label: 'Ручные ножовки по металлу',
          img: 'https://pimdatacdn.bahco.com/media/sub1044/16a916dc6e8c7f3f.png'
        },
        {
          label: 'Ножовки',
          img: 'https://pimdatacdn.bahco.com/media/sub1044/16a916dd3aa2a73f.png'
        },
        {
          label: 'Стамески',
          img: 'https://pimdatacdn.bahco.com/media/sub1044/16a9171d8167303f.png'
        },
        {
          label: 'Ножницы и ножи',
          img: 'https://pimdatacdn.bahco.com/media/sub1044/16a91700f13dab3f.png'
        },
        {
          label: 'Зажимы и тиски',
          img: 'https://pimdatacdn.bahco.com/media/sub1044/16a916bf2d47c13f.png'
        },
        {
          label: 'Молотки',
          img: 'https://pimdatacdn.bahco.com/media/sub1044/16a916dcfd21263f.png'
        },
        {
          label: 'Инструменты для лесоповала и лесозаготовок',
          img: 'https://pimdatacdn.bahco.com/media/sub1044/16a9171ae7ddfc3f.png'
        },
        {
          label: 'Зубила и пробойники',
          img: 'https://pimdatacdn.bahco.com/media/sub1044/16a916c1731e233f.png'
        },
        {
          label: 'Гвоздодеры, монтировки',
          img: 'https://pimdatacdn.bahco.com/media/sub1044/16a916ffff02ba3f.png'
        },
        {
          label: 'Инструменты для культивации и посадки',
          img: 'https://pimdatacdn.bahco.com/media/sub1044/16a916fb5c89d63f.png'
        },
        {
          label: 'Инструменты для подрезки',
          img: 'https://pimdatacdn.bahco.com/media/sub1044/16a917015992523f.png'
        },
        {
          label: 'Пневматические садовые инструменты',
          img: 'https://pimdatacdn.bahco.com/media/sub1044/16a916beb7123b3f.png'
        },
        {
          label: 'Аккумуляторные садовые электроинструменты',
          img: 'https://pimdatacdn.bahco.com/media/sub1044/16a916c2a9b9383f.png'
        },
        {
          label: 'Пинцеты для мелких и электронных деталей',
          img: 'https://pimdatacdn.bahco.com/media/sub5/17358623194a5d3f.png'
        }
      ];
  }

}
