Mongo:
    run locally:
    1) "C:\Program Files\MongoDB\Server\4.2\bin\mongod.exe" --dbpath="c:\data\db"
    2) "C:\Program Files\MongoDB\Server\4.2\bin\mongo.exe"
    
    script:
    
    commands:
    - use dbname
    - db.createCollection("colName)
    - db.colname.drop()
    - db.colName.insertOne({"field1Name":"field1Value","field2Name":"field2Value"})
    - db.colName.insertMany([{"field1Name":"field1Value","field2Name":"field2Value"}, {...}])
    - db.colName.find().limit(2)
    - db.colName.find({age: {$gte: 22}}, {email: "@gmail.com"}, {_id: 0}).limit(10).sort({secondName: 1, firstName: -1})
    
Angular:
    1) cd main\js\application
    2) ng serve --open
    3) open http://localhost:4200/

Swagger:
    http://localhost:8080/swagger-ui.html